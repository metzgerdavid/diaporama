# Mon diaporama
## 1ère partie
---
## 2ème partie
https://youtu.be/WsfFdcqdAsk?feature=shared

<iframe width="1280" height="720" src="https://www.youtube.com/embed/WsfFdcqdAsk" title="La chaîne fonctionnelle : chaîne d&#39;énergie et chaîne d&#39;information" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
---
Les fonctions techniques
<iframe width="1280" height="720" src="https://www.youtube.com/embed/lYZUGpPQwp0" title="Diagramme FAST" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

----
Merci

